;;; This code is in the public domain.
;;;
;;; The preliminary name for this project is SICL, which doesn't stand
;;; for anything in particular.  Pronounce it like "sickle".
;;;
;;; The purpose of this code is to provide a totally portable
;;; implementation of some high-level functionality of the Common Lisp
;;; language, so that implementors of Common Lisp systems can
;;; integrate it as it is into their systems, without having to
;;; implement and maintain a specific version of it. 
;;;
;;; Author: Robert Strandh (strandh@labri.fr)
;;; Date: 2008
;;;
;;; A portable implementation of the Common Lisp
;;; conditional macros
;;; 
;;; This implementation does not use any iteration construct, nor any
;;; operations on sequences (other than the ones we define ourself
;;; here).  Implementations can therefore load this file very early on
;;; in the bootstrap process.
;;;
;;; This implementation also does not use the format function, and
;;; instead uses print and princ for error reporting.  This makes it
;;; possible for format to use the conditional constructs define here.

;;; Ultimately, this form should be moved to a central place, such as
;;; packages.lisp.
(defpackage #:sicl-conditionals
    (:use #:common-lisp)
  ;; Shadow these for now.  Ultimately, import them with
  ;; the rest of the CL package. 
  (:shadow #:or #:and #:when #:unless #:cond)
  (:shadow #:case #:ccase #:ease)
  (:shadow #:typecase #:ctypecase #:etypecase ))

(in-package #:sicl-conditionals)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Conditions

(define-condition malformed-body (program-error)
  ((%body :initarg :body :reader body))
  (:report
   (lambda (condition stream)
     (princ "Expected a proper list of forms," stream)
     (terpri stream)
     (princ "but found: " stream)
     (print (body condition) stream))))
     
(define-condition malformed-cond-body (program-error)
  ((%body :initarg :body :reader body))
  (:report
   (lambda (condition stream)
     (princ "Expected a proper list of cond clauses," stream)
     (terpri stream)
     (princ "but found: " stream)
     (print (body condition) stream))))
     
(define-condition malformed-cond-clause (program-error)
  ((%clause :initarg :clause :reader clause))
  (:report
   (lambda (condition stream)
     (princ "Expected a cond clause of the form," stream)
     (terpri stream)
     (princ "(test-form form*)," stream)
     (terpri stream)
     (princ "but found: " stream)
     (print (clause condition) stream))))
     

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Utilities

;;; Check that an object is a proper list
(defun proper-list-p (object)
  (or (null object)
      (and (consp object)
	   (proper-list-p (cdr object)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Implementation of the macros

(defmacro or (&rest forms)
  (if (null forms)
      nil
      (if (not (consp forms))
	  (error 'malformed-body :body forms)
	  (if (null (cdr forms))
	      (car forms)
	      (let ((temp-var (gensym)))
		`(let ((,temp-var ,(car forms)))
		   (if ,temp-var
		       ,temp-var
		       (or ,@(cdr forms)))))))))

(defmacro and (&rest forms)
  (if (null forms)
      t
      (if (not (consp forms))
	  (error 'malformed-body :body forms)
	  (if (null (cdr forms))
	      (car forms)
	      `(if ,(car form)
		   (and ,(cdr form))
		   nil)))))

(defmacro when (form &body body)
  (if (not (proper-list-p body))
      (error 'malformed-body :body body)
      `(if ,form
	   (progn ,@body)
	   nil)))

(defmacro unless (form &body body)
  (if (not (proper-list-p body))
      (error 'malformed-body :body body)
      `(if ,form
	   nil
	   (progn ,@body))))

(defmacro cond (&rest clauses)
  (if (not (proper-list-p body))
      (error 'malformed-cond-body :body body)
      (if (null clauses)
	  nil
	  (let ((clause (car clauses)))
	    (if (not (and (proper-list-p clause)
			  (not (null clause))))
		(error 'malformed-cond-clause
		       :clause clause)
		(if (null (cdr clause))
		    `(or ,(car clause)
			 (cond ,@(cdr clauses)))
		    `(if ,(car clause)
			 (progn ,@(cdr clause))
			 (cond ,@(cdr clauses)))))))))
